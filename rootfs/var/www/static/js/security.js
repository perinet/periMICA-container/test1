/* Copyright (C) Perinet */
const RELOAD_TIMEOUT = 1250
const host_production_appendix = `During the periNODE production, a unique <i>host certificate</i> is deployed,
which has been signed by Perinet's publicly available Root CA certificate (perinet-ecc-root-ca).`

const host_tool_tip = `During the initiation of the TLS connection, the periNODE authenticates itself by sending a <i>host certificate</i> to the connecting remote HTTPs client.
The <i>host certificate</i> has been signed by a trusted authority, which is also trusted by the remote client. The remote client can therefore be sure to communicate with an authentic periNODE.
` + host_production_appendix + `</br></br><b>Perinet also provides the PKI2go, which can be used as a trusted authority.</b>`

const root_tool_tip = `During the initiation of the TLS connection, the periNODE can request a client authentication as well (mutual TLS).
A remote HTTPs client must authenticate itself by sending a <i>client certificate</i>. The <i>client certificate</i> will be verified with the <i>root certificate</i>,
which has been stored beforehand on the periNODE.
</br></br><b>When mTLS has been activated, only verified clients are allowed to access the periNODE.</b>`
const client_tool_tip = `While accessing an MQTT broker, which is running with mTLS, the periNODE needs to provide a <i>client certificate</i> to be authenticated by the broker.`

function is_node() {
    if(typeof(IS_NODE) === "undefined") {
        return false
    }

    return IS_NODE;
}

class Certificate {
    constructor(id) {
        this.id = id;
        this.cache = "";
    }

    set_cache(c){
        this.cache = c;
    }

    check_cert() {
        const value = document.getElementById(this.id + "-cert").value;
        if (this.id == "client" || this.id == "host") {
            if (!this.cache.includes("-----BEGIN EC PARAMETERS-----")) {
                alert("No private key found in " + capitalize_first_letter(this.id) + " Certificate. Make sure to append it to the certificate string.");
                return false;
            }
        }
        return true
    }
    patch_cert() {
        console.log(this.cache);
        if(is_node()) {
            patch("/security/" + this.id + "-cert", this.cache.replace(/\r?\n|\r/g, ""), "text/plain");
        } else {
            patch("/security/" + this.id + "-cert", this.cache, "text/plain");
        }
    }
}

var certificates = [new Certificate("root"), new Certificate("host")];
var configure_user_role_only = true;
var decoder_triggered = false;


function init_security(add_client_cert) {

    // wait for cert_decode.js to be loaded
    if(is_node() && typeof(Base64) === "undefined") {
        decoder_triggered ? function(){} : load_js_script("js/cert_decoder.js");
        decoder_triggered = true;
        setTimeout( function(){init_security(true)}, 100);
        return;
    }

    create_security_frontend(add_client_cert);
    if(is_node()) {
        document.title = "periNODE Web Server | Security configuration";
    }

    get_ca_cert("root_ca_cert");
    get_cert("host_cert");
    if(add_client_cert) {
        get_cert("client_cert");
        certificates.unshift(new Certificate("client"));
    }
    set_frontend_user_role();
};

function check_error_patch (http, required_role) {
    if (http.status != 204)
    {
        if (http.status == 401) {
            alert("Unauthorized! " + required_role + " Rights are required.");
        }
        else
        {
            alert("Http Status: " + http.status + ", " + http.responseText);
        }
        return false;
    }
    else
    {
        alert("Configuration successful!");
        return true;
    }
}

function upload_metadata_cert(id, file) {
    var obj = document.getElementById(id + "FileName");
    var reader = new FileReader();
    reader.onloadend = function () {
        for (var i=0;i<certificates.length; i++) {
            if(id == certificates[i].id) {
                var old_cache = certificates[i].cache;
                certificates[i].set_cache(reader.result);
                document.getElementById(id + "-cert").value = decode(reader.result);
                if(!push_cert(id)) {
                    document.getElementById(id + "-cert").value = decode(old_cache);
                    certificates[i].set_cache(old_cache);
                }
            }
        }
    }
    reader.readAsText(file);
}

function push_cert(cert_id) {
    var confirm_msg = `Using wrong certificates might make your periNODE inaccessible.

Proceed?`;

    if(!is_node()) {
        confirm_msg = confirm_msg.replaceAll("periNODE", "periMICA container");
    }

    if (confirm(confirm_msg)) {
        for (var i = 0; i < certificates.length; i++){
            if (certificates[i].id == cert_id) {
                if(certificates[i].check_cert()) {
                    certificates[i].patch_cert();
                    return true;
                }
            }
        }
    }

    return false;
};

function set_frontend_user_role() {
    var http = new XMLHttpRequest();
    http.open("GET", "/security/config");
    http.send();
    http.addEventListener('load', function() {
        try {
            var client_auth_method = JSON.parse(http.responseText)["client_auth_method"];
            if( client_auth_method == "mTLS" ) {
				document.getElementById("enforce_client_cert").checked = true;
			} else {
				document.getElementById("enforce_client_cert").checked = false;
			}
        } catch (e) {
            if(document.getElementById("enforce_client_cert")) {
                setTimeout(set_frontend_user_role, 500);
            }
        }
    });
    http.onerror = function(){set_frontend_user_role();};
    if(is_node()) {
        register_request(http);
    }
}

function patch(uri, payload, content_type) {
    var http = new XMLHttpRequest();
    http.open("PATCH", uri);
    http.setRequestHeader("Content-Type", content_type + ";charset=UTF-8");
    http.send(payload);
    http.addEventListener('load', function() {
        if(check_error_patch(http, "Admin")) {
            if(uri.endsWith("host_cert") || uri.endsWith("reset")) {
                setTimeout(function(){location.reload()}, RELOAD_TIMEOUT);
            }

            if(!is_node() && uri.endsWith("/security/config")) {
                setTimeout(function(){location.reload()}, RELOAD_TIMEOUT);
            }
        }

        if(uri.endsWith("cert")) {
            get_cert(uri.split("/")[2]);
        }
    });
    if(is_node()) {
        register_request(http);
    }
}

function get_cert(id) {
    var http = new XMLHttpRequest();
    http.open("GET", "/security/certificates/" + id);
    http.send();
    http.addEventListener('load', function() {
        try {
            if(http.status == 200)
            {
                for (var i = 0; i < certificates.length; i++){
                    if (id == certificates[i].id + "-cert") {
                        certificates[i].set_cache(http.responseText);
                        document.getElementById(id).value = decode(http.responseText);
                    }
                }
            } else {
                if(document.getElementById(id)){
                    get_cert(id);
                }
            }
        } catch (e) {
            if(document.getElementById(id)){
                get_cert(id);
            }
        }
    });
    http.onerror = function(){get_cert(id);};
    if(is_node()) {
        register_request(http);
    }
};

function get_ca_cert(id) {
    var http = new XMLHttpRequest();
    http.open("GET", "/security/trust_anchors/" + id);
    http.send();
    http.addEventListener('load', function() {
        try {
            if(http.status == 200)
            {
                for (var i = 0; i < certificates.length; i++){
                    if (id == certificates[i].id + "-cert") {
                        certificates[i].set_cache(http.responseText);
                        document.getElementById(id).value = decode(http.responseText);
                    }
                }
            } else {
                if(document.getElementById(id)){
                    get_cert(id);
                }
            }
        } catch (e) {
            if(document.getElementById(id)){
                get_cert(id);
            }
        }
    });
    http.onerror = function(){get_cert(id);};
    if(is_node()) {
        register_request(http);
    }
};

function patch_cert(id) {
    var cert = document.getElementById(id).value;
    patch("/security/certificates/" + id, "Admin", cert, "text/plain");
};

function reset_security() {
    var confirm_msg = `A security reset restores the manufacturing state of the host certificate and root certificate and deletes the client certificate. 
mTLS is disabled and each unauthorized client can configure the periNODE. This results in a less secure periNODE.
    
Proceed?`;

    if(!is_node()) {
        confirm_msg = confirm_msg.replaceAll("periNODE", "periMICA container");
    }

    if (confirm(confirm_msg)) {
        patch("/security/reset", "", "text/plain");
    }
};

function capitalize_first_letter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

const config_sanity_check = () => {}

const handle_enforce_client_cert = (event) => {
    const enabling = document.getElementById("enforce_client_cert").checked;
    var confirm_msg = "";
    if (enabling) {
        config_sanity_check();
        confirm_msg = `Please ensure to have a proper root certificate deployed before enabling mTLS. An empty or incompatible root certificate (incapable to verify the client certificates) results in an inaccessible periNODE.

Perinet provides the PKI2go, which is recommended for creating the root certificate, host certificate, as well as the client certificates. For further information please go to the PKI2go documentation.

Proceed?`
    } else {
        confirm_msg = `When mTLS is disabled, all clients are allowed to access the periNODE, without being verified. This results in a less secure periNODE.

Proceed?`
    }

    if(!is_node()) {
        confirm_msg = confirm_msg.replaceAll("periNODE", "periMICA container")
    }

    if (confirm(confirm_msg)) {
		var client_auth_method = "NONE";
		if( enabling == true ) {
			client_auth_method = "mTLS";
		}
        patch("/security/config", JSON.stringify({
            "client_auth_method": client_auth_method
        }), "application/json");
    } else {
        document.getElementById("enforce_client_cert").checked = !enabling;
    }
}

function get_content(table) {
    return `<section>
    <figure>
        <img src="images/security.png" alt="periNODE" width="170">
    </figure>
</section>

<section>
    <h1>Security configuration</h1>
    <table id="security_table">` + table+ `</table>
</section>`;
}

function create_cert_upload_frontend(cert, tool_tip, auth_type) {
    return `<tr>
    <td class="left"><h2>` + auth_type + ` authentication</h2></td>
    <td class="text">
        <div style="position: relative">
            <div class="help-tip"></div>
            <label>
                <p>` + tool_tip + `
                </p>
            </label>
        </div>
    </td>
</tr>
<tr>
    <td class="left">` + capitalize_first_letter( cert ) + ` certificate:</td>
</tr>
<tr>
    <td colspan="2" class="left" style="height:10px"></td>
</tr><tr>
<tr>
    <td class="left"></td>
    <td class="textarea" align="left" style="padding-left:35px;">
        <textarea readonly type="text" id="` + cert + `-cert" style="width:550px; line-height:2; padding:10px; height:350px;"></textarea>
    </td>
</tr>
<tr>
    <td colspan="2" class="left" style="height:18px"></td>
</tr><tr>
<td class="left"></td>
<td style="padding-left: 10px; margin-top:10px" align="left">
    <input type="file" style="display:none;" id="` + cert + `FileName" onchange="upload_metadata_cert('` + cert + `', this.files[0])">
    <input style="!important;text-decoration:none; margin-left:130px;" value="Upload ` + cert + ` certificate" id="upload_file_` + cert + `" type="submit" onclick="document.getElementById('` + cert + `FileName').click();">
    </td>
</tr>`
}

function get_content_table(add_client_cert) {
    var content = "";
    content = content + create_cert_upload_frontend("host", host_tool_tip, "Host");
    content = content + create_cert_upload_frontend("root", root_tool_tip, "Client");

    var content = content + `<tr>
    <td colspan="2" class="left" style="height:18px"></td>
</tr><tr>
    <td class="left" style="vertical-align: middle;">Enforce mTLS access:</td>
    <td class="input">
        <div class="switch" style="padding-left:285px; padding-top:10px" >
            <input id="enforce_client_cert" class="cmn-toggle cmn-toggle-round nextto" type="checkbox">
            <label for="enforce_client_cert" id="enforce_client_cert"></label>
        </div>
    </td>
</tr>
<tr>
    <td class="left"><br></td>
    <td class="input"></td>
</tr>`
if ( add_client_cert ){
    content = content + create_cert_upload_frontend("client", client_tool_tip, "Remote");
}

return content;
}

function create_security_frontend(add_client_cert) {
    var table = get_content_table(add_client_cert);

    if(is_node()) {
        document.getElementById("content_id").innerHTML = get_content(table);
    } else {
        table = table.replaceAll(host_production_appendix, "");
        table = table.replaceAll("periNODE", "periMICA container");
        document.getElementById("security_table").innerHTML = table;
    }

    document.getElementById("security_table").style.paddingRight = "30px";
    document.getElementById("enforce_client_cert").addEventListener("change", handle_enforce_client_cert);
}

const keys = [{"key":"countryName", "out":"C"}, {"key":"organizationName", "out":"O"}, {"key":"stateOrProvinceName", "out":"ST"},
                {"key":"localityName", "out":"L"}, {"key":"organizationalUnitName", "out":"OU"}, {"key":"serialNumber", "out":"serialNumber"},
                {"key":"commonName", "out":"CN"}];
const extKeyUsage = ["TLS Web server authentication", "TLS Web client authentication", "codeSigning", "emailProtection", "timeStamping", "ocspSigning"];
const extKeyUsageOid = "1.3.6.1.5.5.7.3";
const cpsOid = "1.3.6.1.5.5.7.2.1";
const aaiOid = "1.3.6.1.5.5.7.48";
const aaiOidKeys = ["OSCP", "CA Issuers", "Time Stamping", "DVCS", "CA Repository", "Signed Object Repository"];
const indent_chars = "   "

function decode(val) {

    if(val==""){
        return "";
    }

    var der = Base64.unarmor(val);
    var asn1 = ASN1.decode(der, 0);
    var s = asn1.toPrettyString();
    return printCertificate(s);
}

function hex_with_colons (hex) {

    if(hex.length % 2 != 0) {
        hex = "0" + hex
    }
    return hex.replace(/(..)/g, '$1:').slice(0,-1);
}

function t(count) {
    var indent_str = "";
    for (var i=0; i < count; i++ )
    {
        indent_str += indent_chars;
    }

    return indent_str;
}

function print_value(s, indent) {

    var limit = 54;

    if(indent >= 5) {
        limit = 45;
    }

    if(s === undefined) {
        return "";
    }

    if (s.length <= limit) {
        return s;
    }

    if(indent === undefined){
        indent = 1;
    }


    var offset = 0;
    var new_s = ""
    do
    {

        var sub_ = s.substr(offset, limit);
        
        new_s += "\n" + t(indent) + sub_;
        offset += limit;
    } while ( offset <= s.length )

    return new_s;
}

function is_hex_string(s) {
    var re = /[0-9A-Fa-f]/;
    for (var i=0; i<s.length; i++) {
        if(!re.test(s[i])) {
            return false;
        }
    }
    return true;
}



function printCertificate(s) {

    /* TODO:
    - parse Netscape (for periNODE default host cert?)
    - parse keyUsage completely
    - parse Subject Alternative Name completely
    - parse Public Key completely*/
    var s_split = s.split("\n");
    var signature_alg = s_split[2].split(":")[0];
    s_split[2] = s_split[2].split(": ")[1] + ": " +  s_split[2].split(": ")[2]
    var subj_key_identifier = "";
    const version = (parseInt(s_split[0]) + 1).toString();
    var out = "Data:\n" + t(1) + "Version: " + version + " (0x" + s_split[0] + ")";
    var serial = hex_with_colons(BigInt(s_split[1].substr(3)).toString(16).toUpperCase());
    out += "\n" + t(1) + "Serial Number: " + print_value(serial, 2);
    out += "\n" + t(1) + "Signature Algorithm: "+ signature_alg;
    out += "\n" + t(1) + "Issuer: ";

    var n = 2;
    while (!s_split[n].startsWith("Time") && n < 15)
    {
        for (var i = 0; i < keys.length; i++)
        {
            const item = keys[i];
            if (s_split[n].startsWith(item["key"]))
            {
                if(!out.endsWith("Issuer: ")) {
                    out += ", ";
                }
                out += item["out"] + " = " + s_split[n].split(": ")[1];
            }
        }
        n+=1;
    }

    if (!s_split[n].startsWith("Time")) {
        return "";
    }

    out += "\n" + t(1) + "Validity\n" + t(2) + "Not Before: " + s_split[n].split(": ")[1];
    n += 1;
    out += "\n" + t(2) + "Not After: " + s_split[n].split(": ")[1];
    n += 1;
    out += "\n" + t(1) + "Subject: "

    while (n < 25)
    {
        var found = false
        for (var i = 0; i < keys.length; i++)
        {
            const item = keys[i];
            if (s_split[n].startsWith(item["key"]))
            {
                found = true
                if (!out.endsWith("Subject: ")) {
                    out += ", ";
                }
                out += item["out"] + " = " + s_split[n].split(": ")[1];
            }
        }

        if(s_split[n].startsWith("2.5.4")) {
            out += ", " + s_split[n].split(": ")[0] + " = " + s_split[n].split(": ")[1];
            found = true;
        }
        
        if(!found){
            break;
        }
        
        n+=1;
    }

    var key_is_set = false;
    for (var i = n; i < s_split.length; i++)
    {
        var line = s_split[i];
        var next_line = s_split[i+1]
        if(line.includes(":") && line.indexOf(":") != 2) {

            if (line.startsWith("authorityKeyIdentifier")) {
                line = "" + t(2) + "X509v3 Authority Key Identifier:\n" + t(3) + "keyid:" + print_value(hex_with_colons(next_line), 4);

                var has_key;
                value = "";
                do {
                    has_key = false;
                    i += 1;
                    next_line = s_split[i+1];
                    for (var j=0; j< keys.length; j++){
                        if(next_line.startsWith(keys[j]["key"]))
                        {
                            value += "/" + keys[j]["out"] + "=" + next_line.split(": ")[1]
                            has_key = true
                        }
                    }
                } while(has_key)

                value != "" ? line += "\n" + t(3) + "DirName:" + print_value(value, 4) : line = line;

                if(is_hex_string(next_line)) {
                    line += "\n" + t(3) + "serial:" + print_value(hex_with_colons(next_line), 4);
                }
            } else if (line.startsWith("keyUsage")){
                /* todo parse values correctly */
                line = "" + t(2) + "X509v3 Key Usage:" + line.split(": ")[1];
            } else if (line.startsWith("cRLDistributionPoints")){
                line = "" + t(2) + "X509v3 CRL Distribution Points:\n" + t(3) + "Full Name: " + print_value(next_line, 4);
                i += 1;
            } else if (line.startsWith("1.3.6.1.5.5.7.1.1")){
                line = "" + t(2) + "Authority Information Access:\n";
                while (next_line.startsWith(aaiOid))
                {
                    for(var k=0; k < aaiOidKeys.length; k++)
                    {
                        if (next_line.split(": ")[0] == aaiOid + "." + (k + 1).toString())
                        {
                            line += "" + t(3) + "" + aaiOidKeys[k] + " - " + print_value(next_line.split(": ")[1], 4) + "\n";
                        }
                    }

                    i += 1;
                    next_line = s_split[i+1];
                }
                
            } else if (line.startsWith("certificatePolicies")){
                subj_key_identifier = line.split(": ")[1];
                line = "" + t(2) + "X509v3 Certificate Policies:\n" + t(3) + "Policy:";

                n_split = next_line.split(": ")
                if (n_split[0] == "anyPolicy"){
                    line += " X509v3 Any Policy"
                } else {
                    line += " " + n_split[0]
                }

                if (n_split.length == 3) {
                    line += "\n" + t(4) + "User Notice - Explicit Text: " + print_value(n_split[2], 5)
                }

                if (n_split[1] == cpsOid)
                {
                    line += "\n" + t(4) + "CPS: " + print_value(n_split[2], 5);
                    i += 1;
                } else if (s_split[i+2].startsWith(cpsOid)) {
                    line += "\n" + t(4) + "CPS: " + print_value(s_split[i+2].split(": ")[1], 5);
                    i += 2;
                }

            } else if (line.startsWith("basicConstraints")){
                var value = "FALSE";
                if (line.split(": ")[1].toLowerCase().includes("true")) {
                    value = "TRUE";
                }
                line = "" + t(2) + "X509v3 Basic Constraints: \n" + t(3) + "CA:" + value;
            } else if (line.startsWith("subjectKeyIdentifier")){
                subj_key_identifier = line.split(": ")[1];
                line = "" + t(2) + "X509v3 Subject Key Identifier: " + print_value(subj_key_identifier, 3);
            } else if (line.startsWith("subjectAltName")) {

                var type = "DNS";
                type = s_split[i + 1].includes("@") ? "email" : "DNS";

                line = "" + t(2) + "X509v3 Subject Alternative Name: \n" + t(3) + "" + type + ":" + s_split[i + 1];
            } else if (line.startsWith(signature_alg)) {
                line = "Signature Algorithm: " + line.split(": ")[0] + print_value(line.split(": ")[1].substr(3));
            } else if (line.startsWith("rsaEncryption")) { /* TODO: Parse public key meta information */
                var suffix = line.split(": ")[1];
                var hex_string = suffix.substr(27, suffix.length - 42);
                line = "" + t(1) + "Public Key Algorithm: rsaEncryption\n" + t(2) + "RSA Public-Key: (" + ((hex_string.length - 2) * 8 / 3).toString() + " bit)\n" + t(2) + "Modulus:" + print_value(hex_string, 3);
                key_is_set = true;
            } else if (line.startsWith("ecPublicKey")) {
                // line = line.replace("prime256v1: ", "");
                var suffix = line.split(": ");
                var hex_string = suffix[suffix.length - 1].substr(3);
                line = "" + t(1) + "Public Key Algorithm: ecPublicKey\n" + t(2) + "Public Key: (" + ((hex_string.length - 2) * 4 / 3).toString() + " bit)\n" + t(2) + "pub:" + print_value(hex_string, 3);

                if (suffix.length == 3){
                    line += "\n" + t(2) + "ASN1 OID: " + suffix[1];
                }
                key_is_set = true;
            } else if (line.startsWith("clientAuth")) {
                if (s_split[i+1].includes("user")) {
                    line = line.replace("1.2.3.111:", s_split[i+1]);
                    line = line.replace("1.2.3.1:", s_split[i+1]);
                    line = line.replace("1.2.3.11:", s_split[i+1]);
                }
            }else if (line.startsWith("extKeyUsage")) {
                line = "" + t(2) + "X509v3 Extended Key Usage:\n" + t(3) + ""

                for (var j=0; j < extKeyUsage.length; j++) {
                    var oid = extKeyUsageOid + "." + (j+1).toString();
                    if(s_split[i+1].startsWith(oid))
                    {
                        if (line != "" + t(2) + "X509v3 Extended Key Usage:\n" + t(3) + "" ) {
                            line += ", "
                        }
                        line += extKeyUsage[j];
                        s_split[i+1] = s_split[i+1].replace(oid + ": ", "");
                    }
                }

                if(s_split[i+1].startsWith("1.2.3.1"))
                {
                    line = line.replace(extKeyUsage[1], extKeyUsage[1] + " (" + s_split[i+2] + ")");
                    i = i + 2;
                }
            } else {
                line  = "" + t(2) + "" + line.split(": ")[0] + ": " +  print_value(line.split(": ")[1], 3);
            }
            
            if (!out.includes("" + t(1) + "X509v3 Extensions:\n") && key_is_set && !line.includes("Public Key Algorithm"))
            {
                line = "" + t(1) + "X509v3 Extensions:\n" + line;
            }
            
            out += "\n" + line;
        }
    }
    return out
}

if(is_node())
{
    update_content("security", function(){init_security(true);});
}
